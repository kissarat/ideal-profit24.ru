<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\controllers;


use app\behaviors\Access;
use app\helpers\Account;
use app\models\Settings;
use app\modules\bank\models\Node;
use app\modules\bank\models\search\Income;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class NodeController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['open'],
                'manager' => ['create', 'update', 'delete', 'pay']
            ]
        ];
    }

    public function actionIndex($user = null, $min = 0) {
        $query = Node::find();
        if ($user) {
            $query->andWhere(['user_name' => $user]);
        }
        else {
//            $query->andWhere('"time" < :time', [
//                ':time' => time() - $min * 3600 * 24
//            ]);
        }
        return $this->render('index', [
            'min' => $min,
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'time' => SORT_ASC,
                    ]
                ]
            ]),
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    public function actionOpen() {
        $model = new Node();
        if ($model->load(Yii::$app->request->post()) && $model->validate(['amount'])) {
            $amount = $model->amount;
            /** @var \app\models\User $me */
            $me = Yii::$app->user->identity;
            $model->user_name = $me->name;
            if ($amount <= $me->account) {
                $transaction = Yii::$app->db->beginTransaction();
                $me->account -= $amount;
                if ($me->save() && $model->invest()) {
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Success'));
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', $model->__debuginfo());
                }

                return $this->render('view', [
                    'model' => $model,
                ]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Insufficient funds'));
            }
        }
        elseif (!$model->amount) {
            $model->amount = (int) Yii::$app->user->identity->account;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Make user income manually
     * @param $id Node id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionPay($id) {
        $model = $this->findModel($id);
        $balance = Account::get('profit');
        if ($model->amount <= $balance) {
            $transaction = Yii::$app->db->beginTransaction();
            $model->user->account += $model->getIncome();
            if ($model->delete() && Income::make($model) && $model->user->update(false, ['account'])) {
                Account::add('profit', -$model->getIncome());
                $transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Success'));
            }
            else {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $model->__debuginfo());
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Insufficient funds'));
        }

        return $this->redirect(['index']);
    }

    public function actionCreate($id = null) {
        $model = new Node();
        if ($id) {
            $base = $this->findModel($id);
            $model->time = $base->time;
        }
        else {
            $model->time = date(Node::DATETIME_SQL, time());
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                Yii::$app->session->setFlash('error', $model->__debuginfo());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Type model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Node the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Node::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

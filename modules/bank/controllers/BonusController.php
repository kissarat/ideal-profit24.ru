<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\controllers;


use app\modules\bank\models\Bonus;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class BonusController extends Controller {

    public function actionIndex($user = null) {
        $query = Bonus::find()->with('node');
        if ($user) {
            $query->where(['user_name' => $user]);
        }

        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider(['query' => $query]),
            'sort' => [
                'defaultOrder' => [
                    'time' => SORT_DESC
                ]
            ],
            'user' => $user
        ]);
    }
}

<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\controllers;

use app\models\Settings;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use app\modules\bank\models\search\Income as IncomeSearch;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class IncomeController extends Controller {

    public static function index($user = null) {
        $searchModel = new IncomeSearch([
            'user_name' => $user
        ]);
        return [
            'dataProvider' => $searchModel->search(Yii::$app->request->queryParams),
            'searchModel' => $searchModel
        ];
    }

    public function actionIndex($user = null) {
        return $this->render('index', static::index($user));
    }

    public function actionBonuses($user = null) {
        $bonus = Settings::get('common', 'referral');
        $query = (new Query())
            ->from('income')
            ->innerJoin('node', 'node_id = node.id')
            ->innerJoin('user', 'user_name = name')
            ->andWhere('ref_name IS NOT NULL')
            ->select("income.id, income.amount as income, node_id, node.amount as deposit,
            income.time, (income.amount * $bonus) as amount, user_name, ref_name");
        if ($user) {
            $query->andWhere(['user_name' => $user]);
        }
        return $this->render('bonuses', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query
            ]),
            'user' => $user
        ]);
    }
}

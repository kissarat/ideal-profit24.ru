<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\models;

use app\helpers\Account;
use app\models\Settings;
use app\models\User;
use DateTime;
use Yii;
use yii\db\ActiveRecord;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * This is the model class for table "node".
 *
 * @property integer $id
 * @property string $user_name
 * @property number $amount
 * @property integer $time
 *
 * @property User $user
 * @property Node[] $children
 */
class Node extends ActiveRecord {
    const DATETIME = "Y-m-d H:i";
    const DATETIME_SQL = "Y-m-d H:i:s";

    public static function tableName() {
        return 'node';
    }

    public function rules() {
        return [
            [['user_name', 'amount'], 'required'],
            [['user_name'], 'string', 'max' => 24],
            ['open', 'boolean'],
            ['amount', 'number',
                'min' => Settings::get('deposit', 'min'),
                'max' => Settings::get('deposit', 'max')
            ],
            ['amount', 'number',
                'min' => Settings::get('deposit', 'min'),
                'max' => Settings::get('deposit', 'max')
            ]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_name' => Yii::t('app', 'Username'),
            'amount' => Yii::t('app', 'Amount'),
            'time' => Yii::t('app', 'Time'),
            'open' => 'Открыт'
        ];
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->hasOne(User::class, ['name' => 'user_name']);
    }

    public function setUser(User $value) {
        $this->user_name = $value->name;
    }

    public function invest() {
        $amount = $this->amount;
        $bonus = null;
        if ($this->user->ref_name && Bonus::find()->where(['user_name' => $this->user_name])->count() <= 0) {
            $bonus = new Bonus([
                'user_name' => $this->user->ref_name,
                'amount' => $amount * (float) Settings::get('common',
                        User::TEAM == $this->user->status ? 'team_referral' : 'referral')
            ]);
            $this->user->referral->account += $bonus->amount;
            if ($this->user->referral->save()) {
                $amount -= $bonus->amount;
            }
        }
        Account::add('profit', $amount);
        $success = $this->save();
       if ($bonus && $success) {
           $bonus->node_id = $this->id;
            $success = $bonus->save();
        }
        return $success;
    }

    public function __debuginfo() {
        $bundle = [
            'id' => $this->id,
            'time' => date($this->time),
            'user_name' => $this->user_name,
            'account' => $this->user->account
        ];
        if (count($this->errors) > 0) {
            $bundle['errors'] = $this->errors;
        }
        if (count($this->user->errors) > 0) {
            $bundle['user_errors'] = $this->user->errors;
        }
        return json_encode($bundle, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public function __toString() {
        return '#' . $this->id . ' $' . $this->amount;
    }

    public function getIncome() {
        return $this->amount * 1.4;
    }

    public function getDatetime() {
        $time = $this->time ? DateTime::createFromFormat(Node::DATETIME_SQL, $this->time) : new DateTime();
//        throw new \Exception($this->time);
        return $time->format(Node::DATETIME);
    }

    public function setDatetime($value) {
        $time = DateTime::createFromFormat(Node::DATETIME, $value);
        $this->time = $time->format(Node::DATETIME_SQL);
    }
}

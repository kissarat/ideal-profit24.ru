<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 12.10.15
 * Time: 12:40
 */

namespace app\modules\bank\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $user_name
 * @property number $amount
 * @property string $time
 * @property integer $days
 * @property string $start
 */
class NodeView extends ActiveRecord {
    public static function tableName() {
        return 'income_view';
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_name' => Yii::t('app', 'Username'),
            'amount' => Yii::t('app', 'Amount'),
            'start' => Yii::t('app', 'Time'),
            'days' => 'Дней с открытия',
            'time' => 'Последнее начисления',
            'open' => 'Открыт'
        ];
    }
}
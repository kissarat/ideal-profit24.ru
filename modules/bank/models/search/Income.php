<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\models\search;

use Yii;
use yii\data\ActiveDataProvider;
use app\modules\bank\models\Income as IncomeModel;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * Income represents the model behind the search form about `app\models\Archive`.
 */
class Income extends IncomeModel
{
    public $user_name;
    public function rules() {
        return [
            ['user_name' , 'string'],
            [['id', 'node_id', 'amount', 'time'], 'integer'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Income::find()
//            ->with('node')
//            ->with('user');
            ->innerJoin('node', 'node.id = node_id')
            ->innerJoin('user', 'user_name = name');

        if ($this->user_name) {
            $query->andWhere(['user_name' => $this->user_name]);
//            $query->andWhere(['user_name' => $this->user_name]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                    'time' => SORT_DESC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'node_id' => $this->node_id,
            'amount' => $this->amount,
            'time' => $this->time
        ]);

        return $dataProvider;
    }
}

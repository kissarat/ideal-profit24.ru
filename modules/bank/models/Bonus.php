<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\models;


use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer node_id
 * @property string user_name
 * @property number amount
 * @property string time
 */
class Bonus extends ActiveRecord {

    public function rules() {
        return [
            [['id', 'node_id'], 'integer'],
            [['user_name', 'time'], 'string'],
            ['amount', 'number']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'node_id' => 'Депозит',
            'user_name' => 'Спонсор',
            'amount' => 'Количество',
            'time' => 'Время'
        ];
    }

    public function getNode() {
        return $this->hasOne(Node::class, ['id' => 'node_id']);
    }

    public static function count($user_name) {
        return static::find()->where(['user_name' => $user_name])->count();
    }
}

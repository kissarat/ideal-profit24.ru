<?php
/**
 * @link http://zenothing.com/
*/

namespace app\modules\bank\models;

use app\models\Message;
use app\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "income".
 *
 * @property integer $id
 * @property integer $node_id
 * @property number $amount
 * @property integer $time
 *
 * @property Node $node
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Income extends ActiveRecord
{
    public static function tableName() {
        return 'income';
    }

    public function rules() {
        return [
            [['node_id', 'time'], 'required'],
            [['node_id', 'time'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'node_id' => Yii::t('app', 'Investment'),
            'amount' => Yii::t('app', 'Amount'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getNode() {
        return $this->hasOne(Node::class, ['id' => 'node_id']);
    }

    public function getUser() {
        return $this->hasOne(Node::class, ['id' => 'node_id'])->viaTable('user', ['name' => 'user_name']);
    }

    public function getUserName() {
        return $this->node->user_name;
    }

    public static function make(Node $node) {
        $income = new Income([
            'node_id' => $node->id,
            'user_name' => $node->user_name,
            'amount' => $node->getIncome(),
            'time' => $node->time
        ]);
        if ($income->save(false)) {
            return true;
        }
        return false;
    }
}

<?php
use app\models\User;
use app\modules\bank\models\Bonus;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Реферальные начисления';
$columns = [
    'id',
    'time:datetime',
    [
        'attribute' => 'node_id',
        'format' => 'html',
        'value' => function(Bonus $model) {
            return $model->node_id ? Html::a($model->node, ['/bank/node/view', 'id' => $model->node_id]) : null;
        }
    ],
    'amount'
];
if ($user) {
    $this->title .= ' пользователя ' . $user;
    if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->isManager()) {
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
    }
    $this->params['breadcrumbs'][] = ['label' => $user, 'url' => ['/user/view', 'name' => $user]];
    $this->params['breadcrumbs'][] = $this->title;
}
else {
    $columns[] = [
        'attribute' => 'user_name',
        'format' => 'html',
        'value' => function(Bonus $model) {
            return Html::a($model->user_name, ['/user/view', 'name' => $model->user_name]);
        }
    ];
}
?>
<div class="bonus">
    <?php
    if ($user) {
        echo Yii::$app->view->render('@app/views/user/panel', [
            'model' => User::findOne(['name' => $user])
        ]);
    }
    ?>

    <h1><?= $this->title ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns
    ]); ?>
</div>

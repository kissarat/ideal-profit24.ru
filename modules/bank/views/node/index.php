<?php
/**
 * @link http://zenothing.com/
 */

use app\helpers\Account;
use app\models\User;
use app\modules\bank\models\Node;
use app\widgets\Ext;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$user = isset($_GET['user']) ? $_GET['user'] : null;
$title = $this->title = Yii::t('app', 'Deposits');

if ($user) {
    if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->isManager()) {
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
    }
    $this->params['breadcrumbs'][] = ['label' => $user, 'url' => ['/user/view', 'name' => $user]];
    $this->params['breadcrumbs'][] = $this->title;
}

$mine = !Yii::$app->user->getIsGuest() && (Yii::$app->user->identity->isManager() || Yii::$app->user->identity->name == $user);
$manager = !Yii::$app->user->getIsGuest() && Yii::$app->user->identity->isManager();
$balance = Account::get('profit');
$columns = [
    'id',
    [
        'attribute' => 'user_name',
        'format' => 'html',
        'value' => function(Node $model) {
            return Html::a($model->user_name, ['/user/view', 'name' => $model->user_name]);
        }
    ],
    'amount',
    [
        'attribute' => 'time',
        'label' => Yii::t('app', 'Days'),
        'value' => function(Node $model) {
            $time = new DateTime($model->time);
            $time->modify('+ 3 hour');
            $diff = $time->diff(new DateTime());
            return $diff->days;
        }
    ],
    'time:datetime',
    [
        'attribute' => 'open',
        'value' => function(Node $node) {
            return $node->open ? 'Да' : 'Нет';
        }
    ],
    $manager
        ? ['class' => ActionColumn::class]
        : [
        'label' => Yii::t('app', 'Action'),
        'format' => 'html',
        'value' => function(Node $model) {
            return Html::a(Yii::t('app', 'View'), ['view', 'id' => $model->id],
                ['class' => 'btn btn-primary btn-xs']);
        }
    ]
];

?>
<div class="node-index">
    <?= Ext::stamp() ?>
    <?php
    if ($user) {
        echo Yii::$app->view->render('@app/views/user/panel', [
            'model' => User::findOne(['name' => $user])
        ]);
    }
    ?>
    <div class="right">
        <h1><?= $title ?></h1>

        <div class="form-group">
            <?php
            if (!Yii::$app->user->getIsGuest()) {
                if (Yii::$app->user->identity->isManager()) {
                    echo Html::tag('div',
                        Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-primary']),
                        ['class' => 'form-group']);
                    echo implode("\n", [
                        Html::beginForm(null, 'get'),
                        Html::label(Yii::t('app', 'Days')),
                        Html::textInput('min', $min),
                        Html::submitButton(Yii::t('app', 'Show')),
                        Html::endForm()
                    ]);
                }
                else {
                    echo Html::a(Yii::t('app', 'Open'), ['open'], ['class' => 'btn btn-success']);
                }
            }
            ?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'emptyText' => '',
            'showOnEmpty' => false,
            'columns' => $columns,
        ])
        ?>
    </div>

</div>

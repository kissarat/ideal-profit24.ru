<?php
use yii\helpers\Html;

function str_round($value) {
    return preg_replace('|(\.?0+)$|', '', $value);
}

$this->title  = 'Реферальние начисления';

$columns = [
    'time:datetime',
    [
        'attribute' => 'id',
        'label' => 'Бонус',
        'value' => function($model) {
            return str_round($model['amount']);
        }
    ],
    [
        'attribute' => 'id',
        'label' => 'Прибыль',
        'format' => 'html',
        'value' => function($model) {
            return Html::a(str_round($model['income']), ['/bank/income/view', 'id' => $model['id']]);
        }
    ],
    [
        'attribute' => 'node_id',
        'label' => 'Депозит',
        'format' => 'html',
        'value' => function($model) {
            return Html::a(str_round($model['deposit']), ['/bank/node/view', 'id' => $model['node_id']]);
        }
    ]
];

if (!$user) {
    $columns[] = [
        'attribute' => 'user_name',
        'label' => 'Пользователь',
        'format' => 'html',
        'value' => function($model) {
            return Html::a($model['user_name'], ['/user/view', 'name' => $model['user_name']]);
        }
    ];
    $columns[] = [
        'attribute' => 'ref_name',
        'label' => 'Реферал',
        'format' => 'html',
        'value' => function($model) {
            return Html::a($model['ref_name'], ['/user/view', 'name' => $model['ref_name']]);
        }
    ];
}

echo Html::tag('h1', $this->title);

/** @var \yii\data\ActiveDataProvider $dataProvider */
echo \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns
]);

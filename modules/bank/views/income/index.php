<?php
/**
 * @link http://zenothing.com/
 */

use app\models\User;
use app\modules\bank\models\Income;
use app\modules\bank\models\Node;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View
 * @var $searchModel \app\modules\bank\models\search\Income
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$title = Yii::t('app', 'Income');
if (empty($user)) {
    $this->title = $title;
}
$columns = [
    'id',
    [
        'attribute' => 'node_id',
        'format' => 'html',
        'value' => function(Income $model) {
            return Html::a($model->node, ['/bank/node/view', 'id' => $model->node_id]);
        }
    ],
    [
        'attribute' => 'amount',
        'value' => function(Income $model) {
            return (float) $model->amount;
        }
    ]
];

if (!$searchModel->user_name) {
    $columns[] = [
        'label' => 'Пользователь',
        'format' => 'html',
        'value' => function(Income $model) {
            return Html::a($model->node->user_name, ['/user/view', 'name' => $model->node->user_name]);
        }
    ];
}

$columns[] = [
    'attribute' => 'time',
    'format' => 'datetime',
    'contentOptions' => [
        'class' => 'moment'
    ]
];
?>
<div class="archive-index">

    <?php
    if ($searchModel->user_name && empty($user)) {
        echo Yii::$app->view->render('@app/views/user/panel', [
            'model' => User::findOne(['name' => $searchModel->user_name])
        ]);
    }
    ?>

    <div>
        <h1 class="bagatelle"><?= Html::encode($title) ?></h1>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{pager}\n{errors}\n{summary}\n{items}",
            'columns' => $columns,
        ]); ?>
    </div>
</div>

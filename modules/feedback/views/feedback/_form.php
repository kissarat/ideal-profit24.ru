<?php
/**
 * @link http://zenothing.com/
*/

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\feedback\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ('guest' == $model->scenario): ?>
        <?= Html::activeTextInput($model, 'username', ['placeholder' => 'Имя']) ?>
        <?= Html::activeTextInput($model, 'email', ['placeholder' => 'Email']) ?>
    <?php endif ?>

    <?= Html::activeTextInput($model, 'subject', ['placeholder' => 'Тема']) ?>
    <?= Html::activeTextarea($model, 'content', ['placeholder' => 'Сообщения']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord
            ? Yii::t('app', 'Create')
            : Yii::t('app', 'Update'), ['class' => $model->isNewRecord
            ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

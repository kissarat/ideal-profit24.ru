<?php
/**
 * @link http://zenothing.com/
 */

use app\modules\article\models\Article;
use app\widgets\Ext;


/* @var $this yii\web\View */
/* @var $model app\modules\feedback\models\Feedback */

$this->title = 'Обращайтесь по любому вопросу';

if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Feedbacks'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = Yii::t('app', 'Create');
}
?>
<h1><?= $this->title ?></h1>
<div class="feedback-create">
    <?= Ext::stamp() ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

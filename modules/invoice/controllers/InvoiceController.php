<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\invoice\controllers;

use app\behaviors\Access;
use app\behaviors\NoTokenValidation;
use app\models\Record;
use app\models\Settings;
use app\modules\bank\models\Node;
use app\modules\invoice\models\Withdrawal;
use app\modules\invoice\models\Invoice;
use app\modules\invoice\models\search\Invoice as InvoiceSearch;
use Exception;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Transaction;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class InvoiceController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'success' => ['post'],
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::class,
                'plain' => ['success', 'fail', 'index', 'view', 'create'],
                'manager' => ['withdraw', 'update', 'delete']
            ],

            'no_csrf' => [
                'class' => NoTokenValidation::class,
                'only' => ['success', 'fail'],
            ]
        ];
    }

    public function actionIndex($user = null, $scenario = null) {
        $searchModel = new InvoiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user
        ]);
    }

    public function actionView($id) {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            'user' => $this->restrictUser(),
            'journal' => new ActiveDataProvider([
                'query' => Record::find()->where([
                    'type' => 'invoice',
                    'object_id' => $id
                ]),
                'sort' =>  [
                    'defaultOrder' => [
                        'time' => SORT_DESC
                    ]
                ]
            ])
        ]);
    }

    public function actionCreate($scenario = 'payment', $amount = null) {
        $model = new Invoice([
            'user_name' => Yii::$app->user->identity->name,
            'scenario' => $scenario,
            'status' => 'create'
        ]);

        if ($amount) {
            $model->amount = $amount;
        }

        if ($model->load(Yii::$app->request->post())) {
            if ('withdraw' == $model->scenario) {
                $model->amount = - abs($model->amount);
                if (abs($model->amount) > $model->user->account) {
                    Yii::$app->session->setFlash('error', Yii::t('app', Invoice::$statuses['insufficient_funds']));
                    $model->amount = $model->user->account;
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'user' => $this->restrictUser()
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->scenario = 'manage';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $invoice = $this->findModel($id);
        if ('success' == $invoice->status) {
            $invoice->saveStatus('delete');
        }
        else {
            $invoice->delete();
        }

        return $this->redirect(['index']);
    }

    public function actionSuccess($id = null) {
        $transaction = Yii::$app->db->beginTransaction();
        $invoice = $id ? $this->findModel($id) : new Invoice([
            'user_name' => Yii::$app->user->identity->name,
            'amount' => $_POST['PAYMENT_AMOUNT']
        ]);
        $string = $_POST['PAYMENT_ID']
            .':'.$_POST['PAYEE_ACCOUNT']
            .':'.$_POST['PAYMENT_AMOUNT']
            .':'.$_POST['PAYMENT_UNITS']
            .':'.$_POST['PAYMENT_BATCH_NUM']
            .':'.$_POST['PAYER_ACCOUNT']
            .':'. Yii::$app->perfect->hashAlternateSecret()
            .':'.$_POST['TIMESTAMPGMT'];
        $invoice->batch = $_POST['PAYMENT_BATCH_NUM'];
        if (Yii::$app->user->identity->name != $invoice->user_name) {
            $transaction->rollBack();
            throw new ForbiddenHttpException(Yii::t('app', 'You can only change the status of your payments'));
        }
        elseif ('success' == $invoice->status) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Payment has already done previously'));
        }
        elseif (strtoupper(md5($string)) != $_POST['V2_HASH']) {
            $invoice->saveStatus('invalid_hash');
            Yii::$app->session->setFlash('error', Yii::t('app', Invoice::$statuses['invalid_hash']));
        }
        elseif ($invoice->amount != $_POST['PAYMENT_AMOUNT']) {
            $invoice->saveStatus('invalid_amount');
            Yii::$app->session->setFlash('error', Yii::t('app', Invoice::$statuses['invalid_amount']));
        }
        elseif (Settings::get('perfect', 'wallet') != $_POST['PAYEE_ACCOUNT']) {
            $invoice->saveStatus('invalid_receiver');
            Yii::$app->session->setFlash('error', Yii::t('app', 'Wrong recipient {wallet}', [
                'wallet' =>  $_POST['PAYEE_ACCOUNT'],
            ]));
        }
        elseif ('USD' != $_POST['PAYMENT_UNITS']) {
            $invoice->saveStatus('invalid_currency');
            Yii::$app->session->setFlash('error', Yii::t('app', Invoice::$statuses['invalid_currency']));
        }
        else {
            if (Settings::get('deposit', 'auto')) {
                $node = new Node([
                    'amount' => $invoice->amount,
                    'user_name' => $invoice->user_name
                ]);
                if ($invoice->saveStatus('success') && $node->invest()) {
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Payment #{id} completed', [
                        'id' => $invoice->id,
                    ]));
                } else {
                    $transaction->rollBack();
                    $transaction = null;
                    $invoice->saveStatus('fail');
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Cannot save payment')
                        . ': ' . $node->__debuginfo()
                        . ' <br /> ' . $invoice->__debuginfo()
                    );
                }
            }
            else {
                /** @var \app\models\User $user */
                $user = Yii::$app->user->identity;
                $user->account += $invoice->amount;
                $is_new_invoice = $invoice->isNewRecord;
                if ($invoice->saveStatus('success') && $user->save(true, ['account'])) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Payment #{id} completed', [
                        'id' => $invoice->id,
                    ]));
                }
                else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Cannot save payment')
                        . ' : ' . $invoice->__debuginfo());
                }
                return $this->redirect($is_new_invoice
                    ? ['/user/view']
                    : ['/invoice/invoice/view', 'id' => $invoice->id]);
            }
        }

        if ($transaction) {
            $transaction->commit();
        }

        return $this->render('view', [
            'model' => $invoice
        ]);
    }

    public function actionFail($id = null) {
        $invoice = null;
        if ($id) {
            $invoice = $this->findModel($id);
        }

        if ($invoice && Yii::$app->user->identity->name != $invoice->user_name) {
            throw new ForbiddenHttpException(Yii::t('app', 'You can only change the status of your payments'));
        }
        else {
            if ($invoice) {
                $invoice->saveStatus('cancel');
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'You cancel payment'));
        }

        if ($invoice) {
            return $this->render('view', [
                'model' => $invoice
            ]);
        }
        else {
            return $this->redirect(['/home/index']);
        }
    }


    public function actionWithdraw($id) {
        $transaction = Yii::$app->db->beginTransaction();
        $invoice = $this->findModel($id);
        try {
            $invoice->scenario = 'withdraw';
            if ('success' == $invoice->status) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('info', Yii::t('app', 'Payment has already done previously'));
            }
            elseif (abs($invoice->amount) > $invoice->user->account) {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Insufficient funds'));
            }
            else {
                if (!Yii::$app->perfect->id) {
                    static::withdrawSuccessful($invoice, $transaction);
                    return $this->render('view', [
                        'model' => $invoice
                    ]);
                }
                else {
                    $withdrawal = Withdrawal::fromInvoice($invoice);
                    $response = file_get_contents('https://perfectmoney.is/acct/confirm.asp?' . $withdrawal);
                    if (!preg_match('/<h1>(.*)<\/h1>/', $response, $result)) {
                        $invoice->throwJournalException($response);
                    } elseif ('Spend' != $result[1]) {
                        $invoice->throwJournalException($result[1]);
                    } elseif (!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/",
                        $response, $result, PREG_SET_ORDER)
                    ) {
                        $invoice->throwJournalException($response);
                    } else {
                        $info = [];
                        foreach ($result as $row) {
                            $info[$row[1]] = $row[2];
                        }
                        if (isset($info["ERROR"])) {
                            $invoice->throwJournalException($info["ERROR"]);
                        } elseif ($info['PAYMENT_AMOUNT'] != abs($invoice->amount)) {
                            $invoice->throwJournalException(Yii::t('app', 'Invalid amount') . ' ' . $info['PAYMENT_AMOUNT']);
                        } else {
                            $invoice->batch = $info['PAYMENT_BATCH_NUM'];
                            static::withdrawSuccessful($invoice, $transaction);
                        }
                    }
                }
            }
        }
        catch(Exception $ex) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $ex->getMessage());
        }
        return $this->render('view', [
            'model' => $invoice
        ]);
    }

    protected static function withdrawSuccessful(Invoice $invoice, Transaction $transaction) {
        $invoice->status = 'success';
        $invoice->user->account -= abs($invoice->amount);
        if ($invoice->user->save() && $invoice->save()) {
            $transaction->commit();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Payment #{id} completed', [
                'id' => $invoice->id,
            ]));
        }
        else {
            Yii::$app->session->setFlash('error', json_encode(
                array_merge($invoice->user->errors, $invoice->errors),
                JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        }
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoice::findOne($id)) !== null) {
            $model->scenario = $model->amount < 0 ? 'withdraw' : 'payment';
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function restrictUser($default = null) {
        return !Yii::$app->user->getIsGuest() && !Yii::$app->user->identity->isManager()
            ? Yii::$app->user->identity->name
            : $default;
    }
}

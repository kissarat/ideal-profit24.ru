<?php

require_once __DIR__ . '/../config/pdo.php';

$name = $config['components']['db']['username'];
$password = $config['components']['db']['password'];

pg_connect('host=/var/run/postgresql');

try {
    pg_query("DROP DATABASE \"$name\"");
}
catch (Exception $ex) {
    echo $ex->getMessage();
}

try {
    pg_query("DROP ROLE \"$name\"");
}
catch (Exception $ex) {
    echo $ex->getMessage();
}

pg_query("CREATE ROLE \"$name\" LOGIN PASSWORD '$password'");
pg_query("CREATE DATABASE \"$name\" OWNER \"$name\"");

pg_close();

$schema = file_get_contents(ROOT . '/config/schema.sql');
$schema = explode(';', $schema);
$schema[] = file_get_contents(ROOT . '/config/procedure.sql');
$pdo = connect();
$pdo->beginTransaction();
foreach($schema as $sql) {
    $sql = trim($sql);
    if (!empty($sql)) {
        $sql = preg_replace('|/\*[^\*]+\*/|', '', $sql);
        $sql = preg_replace('|\s+|', ' ', $sql);
        $sql = trim($sql);
//        echo $sql . "\n";
        $pdo->exec($sql);
    }
}
$pdo->commit();

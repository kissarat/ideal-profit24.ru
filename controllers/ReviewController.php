<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;


use app\behaviors\Access;
use app\models\Review;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class ReviewController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::className(),
                'plain' => ['create'],
                'manager' => ['update', 'delete']
            ],
        ];
    }

    public function actionIndex($id = null) {
        $models = Review::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('index', [
            'models' => $models,
            'id' => $id
        ]);
    }

    public function actionCreate() {
        $model = new Review();
        if (Yii::$app->user->identity->isManager()) {
            $model->scenario = 'manage';
        }
        return $this->edit($model);
    }

    public function actionUpdate($id) {
        return $this->edit($this->findModel($id));
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Deleted'));
        return $this->redirect(['index']);
    }

    protected function edit(Review $model) {
        if ('default' == $model->scenario) {
            $model->user_name = Yii::$app->user->identity->name;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        }
        return $this->render('edit', [
            'model' => $model
        ]);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var Review $model */
        if (($model = Review::findOne($id)) !== null) {
            if (Yii::$app->user->identity->isManager()) {
                $model->scenario = 'manage';
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

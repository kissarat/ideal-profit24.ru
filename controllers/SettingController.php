<?php

namespace app\controllers;


use app\behaviors\Access;
use app\models\Settings;
use Yii;
use yii\web\Controller;

class SettingController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'admin' => ['index', 'edit'],
            ]
        ];
    }

    public function actionIndex() {
        return $this->render('index', [
            'categories' => Settings::categories()
        ]);
    }

    public function actionEdit($category) {
        $model = new Settings([
            'category' => $category
        ]);
        if (Yii::$app->request->isPost) {
            $model->setup(Yii::$app->request->post('Settings'));
            $model->clearAttributes();
            if ($count = $model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Success'));
            }
        }
        $model->initAttributes();
        return $this->render('edit', [
            'model' => $model
        ]);
    }
}

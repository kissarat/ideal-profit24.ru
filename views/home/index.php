<?php
/**
 * @link http://zenothing.com/
 * @var string $statistics
 */

$this->title = Yii::$app->name;
?>
<div class="home-index">
    <div class="slider-container">
        <ul class="slider">
            <li style="background-image: url('/images/slides/1.jpg')"></li>
            <li style="background-image: url('/images/slides/2.jpg')"></li>
            <li style="background-image: url('/images/slides/3.jpg')"></li>
            <li style="background-image: url('/images/slides/4.jpg')"></li>
            <li style="background-image: url('/images/slides/5.jpg')"></li>
        </ul>
        <div class="signup">
            <h1>Доллар стоит столько, сколько скажет биржа</h1>
            <div>Поддерживай рекламу, и реклама поддержит тебя</div>
            <a class="button" href="/user/signup">Регистрация</a>
        </div>
    </div>
    <div class="text">
        <p>
            Доверительное управление средствами для торговли на валютном рынке Forex лидировало длительное время,
            однако сейчас мы наблюдаем банкротство многих брокеров. Наша группа предлагает более стабильный и
            надежный способ вложения финансов — торговлю коэффициентами на рынке Betfair (до in-play, т.е. до
            начала спортивного события).
        </p>
        <p>
            Спортивные события никак не зависят от мировых новостей и трендов. Вот почему этот вид инвестирования
            более привлекателен. Правильный подход к спортивному рынку Betfair — это не просто молодой вид
            инвестиций для доверительного управления, а реально работающий способ стабильного и хорошего заработка.
        </p>
        <p>Основное преимущество торговли до начала спортивного мероприятия заключается в невозможности «слива»
            депозита, при этом наша стратегия допускает максимальную просадку всего 15%. Мы учитываем
            «Премиум издержки» Betfair и строго следим за выполнениями всех правил спортивного рынка.
        </p>
        <a class="button" href="/home/about">Читать дальше</a>
    </div>
</div>

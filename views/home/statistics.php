<?php
/**
 * @link http://zenothing.com/
 */
use yii\helpers\Html;
?>
<div class="content">
    <?php
    $items = [];
    foreach($statistics as $name => $value) {
        $items[] = Html::tag('span', Yii::t('app', $name) . ':') . Html::tag('span', $value);
    }

    echo Html::ul($items, [
        'encode' => false
    ]);
    ?>
</div>

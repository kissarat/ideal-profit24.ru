<?php
use yii\helpers\Html;
?>
<div class="top">
    <?= Html::tag('h1', $this->title, ['class' => 'blue-border']); ?>
    <table class="table">
        <thead>
        <tr>
            <?php
            /** @var array $headers */
            foreach($headers as $header) {
                echo Html::tag('th', $header);
            }
            ?>
        </tr>
        </thead>
        <tbody>
        <?php
        /** @var array $rows */
        foreach($rows as $row) {
            echo '<tr>';
            foreach($row as $cell) {
                echo Html::tag('td', $cell);
            }
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
</div>

<?php
/**
 * @link http://zenothing.com/
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var string $user
 */

use app\models\Visit;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Visits');
if ($user) {
    $this->title .= ': ' . $user;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Visits'), 'url' => ['visit']];
    $this->params['breadcrumbs'][] = $user;
}

echo Html::tag('h1', $this->title);
echo Html::tag('div', implode("\n", [
    Html::a('Браузеры', ['user-agents'], ['class' => 'btn btn-primary'])
]),
    ['class' => 'form-group']);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'time:datetime',
        [
            'attribute' => 'spend',
            'format' => 'html',
            'value' => function(Visit $model) {
                $interval = new DateInterval('PT' . floor($model->spend / 60) . 'M' . ($model->spend % 60) . 'S');
                return Html::a($interval->format('%I:%S'), ['data', 'id' => $model->id]);
            }
        ],
        [
            'attribute' => 'user_name',
            'format' => 'html',
            'value' => function(Visit $model) {
                return $model->user_name ? Html::a($model->user_name, ['visit', 'user' => $model->user_name]) : null;
            }
        ],
        [
            'attribute' => 'path',
            'format' => 'html',
            'value' => function(Visit $model) {
                return Html::a('/' . $model->path, 'https://ideal-profit24.ru/' . $model->path);
            }
        ],
        'ip',
        [
            'attribute' => 'agent',
            'format' => 'html',
            'contentOptions' => [
                'class' => 'small'
            ],
            'value' => function(Visit $model) {
                return Visit::getStrongUserAgent($model->agent);
            }
        ]
    ]
]);

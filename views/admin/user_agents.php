<?php
/**
 * @link http://zenothing.com/
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var string $user
 */

use app\models\Visit;
use yii\grid\GridView;
use yii\helpers\Html;

$ip = isset($ip) ? $ip : null;

$this->title = Yii::t('app', 'User Agents');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Visits'), 'url' => ['visit']];
if ($ip) {
    $this->title = $ip . ' ' . $this->title;
    $this->params['breadcrumbs'][] = ['label' => 'Браузеры', 'url' => ['user-agents']];
}

echo Html::tag('h1', $this->title);
$columns = [[
    'attribute' => 'agent',
    'format' => 'html',
    'contentOptions' => $ip ? [] : [
        'class' => 'small'
    ],
    'value' => function(array $model) use ($ip) {
        return $ip ? $model['agent'] : Visit::getStrongUserAgent($model['agent']);
    }
]];

if (!$ip) {
    array_unshift($columns, [
        'attribute' => 'ip',
        'label' => 'IP',
        'format' => 'html',
        'value' => function(array $model) {
            return Html::a($model['ip'], ['user-agents', 'ip' => $model['ip']]);
        }
    ]);
}

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns
]);

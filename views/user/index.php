<?php
/**
 * @link http://zenothing.com/
 */

use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (isset($_GET['ref_name'])) {
    $this->title = 'Партнеры пользовател ' . $_GET['ref_name'];
}
else {
    $this->title = Yii::t('app', 'Users');
}

$columns = [
    [
        'attribute' => 'name',
        'format' => 'html',
        'value' => function(User $model) {
            return Html::a($model->name, ['view', 'name' => $model->name]);
        }
    ],
    'account',
    [
        'attribute' => 'ref_name',
        'format' => 'html',
        'value' => function(User $model) {
            return $model->ref_name ? Html::a($model->ref_name, ['index', 'ref_name' => $model->ref_name]) : null;
        }
    ],
    [
        'attribute' => 'last_access',
        'format' => 'datetime',
        'contentOptions' => [
            'class' => 'moment'
        ]
    ]
];

if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->isAdmin()) {
    $columns[] = 'email:email';
    $columns[] = [
        'format' => 'html',
        'contentOptions' => ['class' => 'action'],
        'value' => function($model) {
            return implode(' ', [
                Html::a('', ['update', 'name' => $model->name], ['class' => 'glyphicon glyphicon-pencil']),
//                Html::a('', ['delete', 'id' => $model->id], ['class' => 'glyphicon glyphicon-trash'])
            ]);
        }
    ];
}
?>
<div class="user-index">
    <div>
        <h1 class="bagatelle"><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?php
            if (Yii::$app->user->identity->isAdmin()) {
                echo Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']);
            }
            ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $columns,
        ]); ?>
    </div>
</div>

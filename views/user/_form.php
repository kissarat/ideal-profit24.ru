<?php
/**
 * @link http://zenothing.com/
 */

use app\models\User;
use app\widgets\AjaxComplete;
use app\widgets\Ext;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */

echo Ext::stamp();

$form = ActiveForm::begin();

if ($model->ref_name && $model->isNewRecord): ?>
    <div class="form-group">
        <?= Yii::t('app', 'Referral') . ': ' . $model->ref_name; ?>
    </div>
    <?= Html::activeHiddenInput($model, 'ref_name') ?>
<?php endif;
if ($model->isNewRecord) {
    echo Html::activeTextInput($model, 'name', ['placeholder' => 'ЛОГИН']);
}

if ('signup' == $model->scenario):
    echo Html::activeTextInput($model, 'email', ['placeholder' => 'E-MAIL']);
    echo Html::activePasswordInput($model, 'password', ['placeholder' => 'ПАРОЛЬ']);
    echo Html::activePasswordInput($model, 'repeat', ['placeholder' => 'ПОВТОРИТЕ ПАРОЛЬ']);
    echo Html::activeTextInput($model, 'perfect', ['placeholder' => 'КОШЕЛЕК PM']);
endif;

if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
    echo $form->field($model, 'account');
    echo $form->field($model, 'perfect');
    echo $form->field($model, 'status')->dropDownList(User::statuses());
    echo $form->field($model, 'ref_name')->widget(AjaxComplete::class, [
        'route' => ['/user/complete']
    ]);
}

?>
<div class="buttons">
    <?php

    if (Yii::$app->user->isGuest) {
        echo Html::submitButton('Регистрация');
    }
    else {
        echo Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Create' : 'Update'));
    }

    ActiveForm::end();
    ?>
</div>

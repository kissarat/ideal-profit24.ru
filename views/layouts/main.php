<?php
/**
 * @link http://zenothing.com/
 */

use app\helpers\MainAsset;
use app\models\Settings;
use app\modules\bank\models\Node;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

$browser = strtolower($_SERVER['HTTP_USER_AGENT']);
$description = false === strpos($browser, 'wayback') && false === strpos($browser, 'bot') && false === strpos($browser, 'wget') ? null :
    'Предупреждения! Данний HyIP, вероятно, являеться СКАМом, посколько администрация все еще не оплатила разарботку сайта $140';

if ($description) {
    $this->registerMetaTag([
        'name' => 'description',
        'content' => $description
    ]);
}

MainAsset::register($this);
$login = Yii::$app->user->isGuest ? '' : 'login';
$manager = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();
$route = [Yii::$app->controller->id, Yii::$app->controller->action->id];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>"><!--

Домен зарегистрирован на имя разработчика и хоститься на его (разработчика) серверах,
но разработчик не являеться владельцем сайта и не несет ответственности за его действия,
т.е. разработчик не несет ответственности за действия владельца сайта,
которые, в том числе, включают в себя проведения любых денежных операций
связанных с кошельком Perfect Money <?= Settings::get('perfect', 'wallet') .
"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n
\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" ?>
-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="image_src" href="/images/cover.png" />
    <?php $this->head() ?>
    <?= Html::csrfMetaTags() ?>
</head>
<body class="<?= implode(' ', $route) ?>">
<div id="background"></div>
<?php $this->beginBody();
if (!Yii::$app->user->getIsGuest()) {
    $user = Yii::$app->user->identity->name;
    echo Html::script("var user = '$user'");
}
?>
<div class="wrap <?= $login ?>">
    <header class="background">
        <div class="header-container">
            <div>
                <img id="logo" src="/images/logo.png" />
                <strong>IDEAL-PROFIT24</strong>
            </div>
            <div>
                <div class="chat">
                    <?= Html::a(Html::img('/images/icons/skype-crystal.png'), Settings::get('common', 'skype')) ?>
                </div>
                <div class="auth">
                    <div>
                        <?php
                        $items = [];
                        if (Yii::$app->user->getIsGuest()) {
                            $items[] = Html::a(Yii::t('app', 'Login'), ['/user/login']);
                            $items[] = Html::a(Yii::t('app', 'Signup'), ['/user/signup']);
                        } else {
                            $items[] = Html::a(Yii::t('app', 'Logout'), ['/user/logout']);
                            $items[] = Html::a(Yii::t('app', 'Profile'), ['/user/view']);
                        }
                        echo implode("", $items);
                        ?>
                    </div>
                    <img src="/images/shadow.png" />
                </div>
            </div>
        </div>
        <?php
        NavBar::begin();

        $items = [
            ['label' => Yii::t('app', 'Home'), 'url' => ['/home/index'], 'options' => ['class' => 'hideable']],
            ['label' => Yii::t('app', 'About'), 'url' => ['/home/about']],
            ['label' => Yii::t('app', 'Investments'), 'url' => ['/home/marketing']],
            ['label' => Yii::t('app', 'FAQ'), 'url' => ['/faq/faq/index']],
        ];

        if ($manager) {
            $items[0] = ['label' => Yii::t('app', 'Admin Panel'),
                'url' => 'http://admin.' . $_SERVER['HTTP_HOST'] . '/bank/node/index',
                'options' => [
                    'data' => [
                        'method' => 'post',
                        'params' => [
                            'auth' => Yii::$app->user->identity->auth
                        ]
                    ]
                ]
            ];
        }

        $items[] = ['label' => Yii::t('app', 'Contacts'), 'url' => ['/feedback/feedback/create']];
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => $items,
        ]);
        NavBar::end();
        ?>
    </header>
    <div style="text-align: center">
        <?= Html::a('', ['/page/marketing'], ['class' => 'start']) ?>
    </div>
    <?= Breadcrumbs::widget([
        'homeLink' => false,
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    <?php
    if ($description) {
        echo "<div class=\"alert alert-danger\">$description</div>";
    }

    if (!Yii::$app->user->getIsGuest()) {
        if (Yii::$app->user->identity->isManager()) {
            echo "<div class=\"alert alert-warning\">Ей, админ, где ты? Где моих $140? Это разработчик Тарас Лабяк. Данное сообщения видит только администрация сайта</div>";
        }
    }
    ?>
    <div class="background">
        <?= $content ?>
    </div>

    <?php require_once 'linux.php' ?>
    <footer class="container">
        Разработано <a href="http://zenothing.com/">zenothing.com</a>
    </footer>
</div>

<?php $this->endBody() ?>

<?php if (Settings::get('common', 'heart')): ?>
    <script>
        (function(){
            var widget_id = <?= Settings::get('common', 'heart') ?>;
            _shcp =[{widget_id : widget_id}];
            var lang =(navigator.language || navigator.systemLanguage
            || navigator.userLanguage ||"en")
                .substr(0,2).toLowerCase();
            var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/"+ lang +"/widget.js";
            var hcc = document.createElement("script");
            hcc.type ="text/javascript";
            hcc.async =true;
            hcc.src =("https:"== document.location.protocol ?"https":"http")
                +"://"+ url;
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hcc, s.nextSibling);
        })();
    </script>
<?php endif ?>

<?php if (Settings::get('common', 'google')): ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '<?= Settings::get('common', 'google') ?>', 'auto');
        ga('require', 'linkid', 'linkid.js');
        ga('send', 'pageview');
    </script>
<?php endif ?>
</body>
</html>
<?php $this->endPage() ?>

<?php
/**
 * @link http://zenothing.com/
 */

use app\helpers\AdminAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

if (Yii::$app->user->getIsGuest()
    && !('user' == Yii::$app->controller->id && 'login' == Yii::$app->controller->action->id)) {
    Yii::$app->response->redirect(['/user/login']);
}

AdminAsset::register($this);
$login = Yii::$app->user->getIsGuest() ? '' : 'login';
$manager = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();
$admin = !Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="image_src" href="/img/cover.png" />
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body class="<?= Yii::$app->controller->id . ' ' . Yii::$app->controller->action->id ?>">
<?php $this->beginBody() ?>
<div class="wrap <?= $login ?> admin">
    <nav>
        <?php
        $items = [
            ['label' => Yii::t('app', 'Site'),
                'url' => 'http://' . substr($_SERVER['HTTP_HOST'], 6),
                'options' => [
                    'data' => [
                        'method' => 'post',
                        'params' => [
                            'auth' => $manager && Yii::$app->user->identity->auth
                        ]
                    ],
                    'class' => 'icon site'
                ]
            ],
            ['label' => Yii::t('app', 'Statistics'), 'url' => ['/home/statistics'],
                'options' => ['class' => 'icon statistics']],
            ['label' => Yii::t('app', 'Feedback'), 'url' => ['/feedback/feedback/index'],
                'options' => ['class' => 'icon feedback']],
            ['label' => Yii::t('app', 'FAQ'), 'url' => ['/faq/faq/index'],
                'options' => ['class' => 'icon reviews']],
//            ['label' => Yii::t('app', 'Reviews'), 'url' => ['/review/index'],
//                'options' => ['class' => 'icon reviews']],
//            ['label' => Yii::t('app', 'News'), 'url' => ['/article/article/index'],
//                'options' => ['class' => 'icon news']],
//            ['label' => Yii::t('app', 'Pages'), 'url' => ['/article/article/pages'],
//                'options' => ['class' => 'icon pages']],
        ];

        if (Yii::$app->user->isGuest) {
            $items[] = ['label' => Yii::t('app', 'Signup'), 'url' => ['/user/signup']];
            $items[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/user/login']];
        }
        else {
            $items[] = ['label' => Yii::t('app', 'Payments'), 'url' =>['/invoice/invoice/index'],
                'options' => ['class' => 'icon payments']];
            $items[] = ['label' => Yii::t('app', 'Journal') , 'url' =>['/journal/index'],
                'options' => ['class' => 'icon journal']];
            if ($manager) {
                $items[] = ['label' => Yii::t('app', 'Users'), 'url' => ['/user/index'],
                    'options' => ['class' => 'icon users']];
                $items[] = ['label' => Yii::t('app', 'Income'), 'url' => ['/bank/income/index'],
                    'options' => ['class' => 'icon income']];
                $items[] = ['label' => Yii::t('app', 'Investments'), 'url' => ['/bank/node/index'],
                    'options' => ['class' => 'icon investments']];
                $items[] = ['label' => 'Реферальные начисления', 'url' => ['/bank/bonus/index']];
                $items[] = ['label' => Yii::t('app', 'Accounts'), 'url' => ['/user/account'],
                    'options' => ['class' => 'icon accounts']];
                $items[] = ['label' => Yii::t('app', 'Translation') , 'url' => ['/lang/lang/index'],
                    'options' => ['class' => 'icon translation']];
                $items[] = ['label' => Yii::t('app', 'Visits'), 'url' => ['/admin/visit'],
                    'options' => ['class' => 'icon visits']];
            }
            if ($admin) {
                $items[] = ['label' => Yii::t('app', 'Settings'), 'url' => ['/setting/index'],
                    'options' => ['class' => 'icon settings']];
                $items[] = ['label' => Yii::t('app', 'Import'), 'url' => ['/admin/import'],
                    'options' => ['class' => 'icon import']];
            }
            $items[] = ['label' => Yii::t('app', 'Logout'), 'url' => ['/user/logout'],
                'options' => ['class' => 'icon logout']];
        }

        foreach($items as $item) {
            echo Html::a($item['label'], $item['url'], isset($item['options']) ? $item['options'] : null);
        }
        ?>
    </nav>

    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

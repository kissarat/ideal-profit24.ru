<?php
use yii\helpers\Html;
?>
<div id="linux">
    <div>
        <img src="/images/linux.png" alt="Linux" />
    </div>
    <?= Html::tag('div', Yii::t('app', 'Welcome, Linux user. We are glad you use open source software!'), [
        'class' => 'welcome'
    ]) ?>
    <div class="glyphicon glyphicon-remove"></div>
</div>

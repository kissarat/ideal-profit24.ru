<?php
/**
 * @link http://zenothing.com/
 */

$start = microtime();
if (empty($_GET['spend'])
    || empty($_SERVER['HTTP_REFERER'])) {
    http_response_code(400);
    exit;
}
$ip = $_SERVER['REMOTE_ADDR'];
$host = (empty($_SERVER['HTTPS']) ? 'http' : 'https') . '://' . $_SERVER['HTTP_HOST'] . '/';
$path = $_SERVER['HTTP_REFERER'];
if (0 === strpos($path, $host)) {
    $path = str_replace($host, '', $path);
}
$spend = (int) $_GET['spend'];

header('Content-Type: application/json');
define('CONFIG', __DIR__ . '/../config');

require CONFIG . '/common.php';
require CONFIG . '/web.php';
require CONFIG . '/local.php';

$db = $config['components']['db'];
$pdo = new PDO($db['dsn'], $db['username'], $db['password'], [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
]);

$st = $pdo->prepare('SELECT id FROM "visit_agent" WHERE "agent" = :agent AND "ip" = :ip');
$st->execute([
    ':agent' => $_SERVER['HTTP_USER_AGENT'],
    ':ip' => $ip
]);
$agent_id = $st->fetchColumn();

if (!$agent_id) {
    $st = $pdo->prepare('INSERT INTO "visit_agent"("agent", "ip") VALUES (:agent, :ip) RETURNING id');
    $st->execute([
        ':agent' => $_SERVER['HTTP_USER_AGENT'],
        ':ip' => $ip
    ]);
    $agent_id = $st->fetchColumn();
}

$st = $pdo->prepare('INSERT INTO "visit_path"("agent_id", "path", "spend", "data") VALUES (:agent_id, :path, :spend, :data)');
$st->execute([
    ':agent_id' => $agent_id,
    ':path' => $path,
    ':spend' => floor($spend / 1000),
    ':data' => 'POST' == $_SERVER['REQUEST_METHOD'] ? file_get_contents('php://input') : null
]);

if (!empty($_GET['user'])) {
    $st = $pdo->prepare('UPDATE "user" SET "last_access" = CURRENT_TIMESTAMP WHERE "name" = :name');
    $st->execute([
        ':name' => $_GET['user']
    ]);
}

echo json_encode([
    'agent_id' => $agent_id,
    'spend' => microtime() - $start
]);

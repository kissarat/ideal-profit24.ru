/**
 * @link http://zenothing.com/
 * @author Taras Labiak <kissarat@gmail.com>
 */

var start = Date.now();

function $$(selector) { return document.querySelector(selector) }
function $id(id) { return document.getElementById(id) }
function $all(selector) { return document.querySelectorAll(selector) }
function $class(name) { return document.getElementsByClassName(name) }
function $new(name) { return document.createElement(name) }
function $content(name, content) {
    var div = document.createElement(name);
    div.innerHTML = content;
    return div;
}
function $option(value, text) {
    var option = $new('option');
    option.value = value;
    option.innerHTML = text;
    return option;
}

function $a(label, url) {
    var a = $new('a');
    a.setAttribute('href', url);
    a.innerHTML = label;
    return a;
}

function $row() {
    var row = $new('tr');
    for(var i = 0; i < arguments.length; i++) {
        var cell = arguments[i];
        if (cell instanceof HTMLTableCellElement) {
            row.appendChild(cell);
        }
        else if (cell instanceof Element) {
            row.insertCell(-1).appendChild(cell);
        }
        else if ('string' == typeof cell) {
            row.insertCell(-1).innerHTML = cell;
        }
        else {
            row.insertCell(-1).innerHTML = cell.toString();
        }
    }
    return row;
}

function pg_time(string) {
    return moment(string, 'YYYY-MM-DD H:mm').format('DD-MM-YYYY H:M');
}

function $name(model, name) {
    $$(model + '[' + name + ']');
}

function each(array, call) {
    Array.prototype.forEach.call(array, call);
}

function array(a) {
    return Array.prototype.slice.call(a);
}

function $each(selector, call) {
    if ('string' == typeof selector) {
        selector = $all(selector);
    }
    return each(selector, call);
}

function options(select, call) {
    $each('option', call);
}

function script(source) {
    var element = document.createElement('script');
    element.setAttribute('src', source);
    document.head.appendChild(element);
    return element;
}

var admin = 0 === location.hostname.indexOf('admin.');

//Feature detection
if (document.body.dataset) {
    $each('[data-selector]', function(source) {
        document.querySelector(source.dataset.selector).innerHTML = source.innerHTML;
        source.remove();
    })
}

var $duration = $$('[name="User[duration]"]');
var russian = /lang=en/.test(document.cookie) ? 0 : 1;

Object.defineProperty(Element.prototype, 'visible', {
    get: function() {
        return 'none' != this.style.display
    },
    set: function(value) {
        if (value) {
            this.style.removeProperty('display');
        }
        else {
            this.style.display = 'none';
        }
    }
});

var $remember = $$('[name="LoginForm[remember]"]');

if ($remember) {
    $$('[name="LoginForm[duration]"]').setAttribute('type', 'number');
    $('[name="LoginForm[remember]"]').change(function () {
        duration.visible = !duration.visible;
    });

    $('.user-login [name=select]').change(function () {
        if (!(duration_minutes.visible = '0' == this.value)) {
            $$('[name="LoginForm[duration]"]').value = this.value;
        }
    });
}

function browser(string) {
    return navigator.userAgent.indexOf(string) >= 0;
}

var $footer = $$('.footer');
if ($footer && browser('Windows')) {
    $footer.remove();
}

function report(async) {
    var request = new XMLHttpRequest();
    var params = {spend: window.performance ? performance.now() : Date.now() - start};
    if (window.user) {
        params['user'] = user;
    }
    var url = '/visit.php?' + $.param(params);
    request.open('POST', url, async);
    if (window.performance) {
        trace.state.memory = performance.memory.usedJSHeapSize;
        trace.timing = performance.timing;
    }
    request.send(store('trace'));
}

function store(name) {
    if (!window['_' + name]) {
        window['_' + name] = JSON.stringify(window[name]);
    }
    return window['_' + name];
}

function store_mouse(type) {
    return function(e) {
        trace.mouse[e.timeStamp.toString(32)] = {
            _type: type,
            x: e.clientX,
            y: e.clientY
        }
    }
}

if ('classList' in HTMLElement.prototype) {
    var moments = $class('moment');
    if (moments.length > 0) {
        script('http://momentjs.com/downloads/moment-with-locales.js').onload = function() {
            moment.locale('ru-RU');
            each(moments, function(m) {
                var time = moment(m.textContent, 'DD-MM-YYYY hh:mm');
                var duration = moment.duration(- time.diff());
                if (duration.asHours() < 18) {
                    m.textContent = time.fromNow();
                    if (duration.asMinutes() < 15) {
                        m.classList.add('online');
                    }
                }
            });
        }
    }
}

if (admin) {
    $('._blank').each(function(i, a) {
        a.setAttribute('target', '_blank');
    });

    if ('false' != localStorage.reload) {
        var $d = $(document.body);
        if ($d.hasClass('index') && ($d.hasClass('user')
            || $d.hasClass('invoice') || $d.hasClass('journal')
            || $d.hasClass('income') || $d.hasClass('node'))
            || ($d.hasClass('admin') && $d.hasClass('visit'))) {
            setTimeout(function () {
                location.reload();
            }, 30000);
        }
    }
}
else {
    addEventListener('beforeunload', report);

    $each('[data-role="manage"]', function(element) {
        element.remove();
    });
    /*
     var $linux = $$('#linux');
     if (window.localStorage && $linux && !localStorage.getItem('linux') && browser('Linux') && !browser('Android')) {
     $linux.onclick = function () {
     localStorage.setItem('linux', true);
     this.remove();
     };
     if (browser('Ubuntu')) {
     $linux.querySelector('img').setAttribute('src', '/images/ubuntu.png');
     var $welcome = $linux.querySelector('.welcome');
     $welcome.innerHTML = $welcome.innerHTML.replace('Linux', 'Ubuntu');
     }
     $linux.style.display = 'table-row';
     }
     */
    function slide(slider) {
        var style = getComputedStyle(slider);
        var frames = slider.querySelectorAll('li');
        var left = 0;
        var width = parseFloat(style.width);
        each(frames, function(frame) {
            frame.style.width = style.width;
            frame.style.height = style.height;
            frame.style.left = left + 'px';
            left -= width;
        });

        var queue = array(frames);

        setInterval(function() {
            each(queue, function(frame) {
                frame.classList.add('animated');
                frame.style.left = (parseFloat(frame.style.left) + width) + 'px';
            });
            var part = queue.slice(1);
            left = 0;
            if (queue.length > 0) {
                queue[queue.length - 1].classList.remove('animated');
                each(part, function(frame) {
                    frame.style.left = left + 'px';
                    left -= width;
                });
                part.push(queue[0]);
            }
            queue = part;
        }, 5000);
    }

    $each('.slider', slide);

    var trace = {
        _type: 'trace',
        _id: start,
        _version: 1,
        state: {
            width: innerWidth,
            height: innerHeight,
            referer: document.referer
        },
        mouse: {},
        keyboard: {},
        scroll: {}
    };

    if (window.addEventListener) {
        // addEventListener('mousemove', store_mouse('move'));
        addEventListener('mousedown', store_mouse('down'));
        addEventListener('mouseup', store_mouse('up'));

        addEventListener('keyup', function(e) {
            var flag = 0;
            if (e.ctrlKey)  { flag |= 1; }
            if (e.altKey)   { flag |= 2; }
            if (e.shiftKey) { flag |= 4; }
            if (e.metaKey)  { flag |= 8; }
            if (e.keyCode) {
                trace.keyboard[e.timeStamp.toString(32)] = {
                    _type: 'keyup',
                    code: e.keyCode,
                    flag: flag
                }
            }
        });

        addEventListener('scroll', function(e) {
            trace.scroll[e.timeStamp.toString(32)] = {
                _type: 'scroll',
                y: scrollY
            }
        });

        addEventListener('unload', function() {
            store('_trace');
        });
    }
}

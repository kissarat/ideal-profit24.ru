<?php
/**
 * @link http://zenothing.com/
 */

//header('Content-Type: text/plain');
//echo 'Сайт временно не работает';
//exit;

//if ('ideal-profit24.ru' == $_SERVER['HTTP_HOST'] && empty($_SERVER['HTTPS'])) {
//    header('Location: https://ideal-profit24.ru' . $_SERVER['REQUEST_URI']);
//    exit;
//}

$file = $_SERVER['REQUEST_URI'];
if (0 === strpos($file, '/wp-')) {
    $content = file_get_contents('http://wordpress.org' . $file);
    foreach($http_response_header as $header) {
        header($header);
    }
    echo preg_replace('|<a([^>]+)https?://wordpress.org|', '<a$1http://' . $_SERVER['HTTP_HOST'], $content);
    exit;
}

define('CONFIG', __DIR__ . '/../config');

use yii\web\Application;

require_once CONFIG . '/boot.php';
require_once CONFIG . '/web.php';

$app = new Application($config);
$app->run();

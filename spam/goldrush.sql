create view contact AS
  select `name`, account, email, perfect, phone, skype, sum(amount) as amount FROM `user`
  JOIN invoice ON `name` = user_name
  WHERE amount > 0
  GROUP BY `name`
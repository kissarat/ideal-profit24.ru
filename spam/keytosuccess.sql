create view contact as
  select username as name, email, account, `first`, `last`, skype, phone,
    sum(account) as paid FROM `user`
  join invoice on `user`.id = invoice.sender_id GROUP BY `user`.id;

<?php
/**
 * @link http://zenothing.com/
*/
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
define('ROOT', __DIR__ . '/..');

require_once ROOT . '/vendor/autoload.php';
require_once ROOT . '/vendor/yiisoft/yii2/Yii.php';
require_once __DIR__ . '/common.php';
require_once __DIR__ . '/local.php';

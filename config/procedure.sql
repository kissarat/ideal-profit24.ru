START TRANSACTION;

CREATE OR REPLACE FUNCTION cron_bank() RETURNS INT AS $$
DECLARE
  _income FLOAT;
  _days INT;
  result INT;
BEGIN
  _income := (SELECT "value" FROM setting WHERE "category" = 'deposit' AND "name" = 'income');

  CREATE TEMPORARY TABLE cron_bank AS
    SELECT node_id as id, user_name, income_view.amount * _income as "amount", "start",
           extract(day FROM NOW() - "start") as "days"
    FROM income_view JOIN income ON income_view.id = node_id;
  result := (SELECT count(*) FROM cron_bank);

  IF result > 0 THEN
    INSERT INTO income(node_id, amount, "time")
      SELECT id, amount, "start" + (days * INTERVAL '1 day') FROM cron_bank;

    UPDATE "user" SET account = account + amount
    FROM (SELECT user_name, sum(amount) as amount FROM cron_bank GROUP BY user_name) i
    WHERE "name" = i.user_name;

    _days := (SELECT "value" FROM setting WHERE "category" = 'deposit' AND "name" = 'days');
    UPDATE "node" SET "open" = FALSE FROM (SELECT * FROM cron_bank) t
    WHERE days >= _days;
  END IF;

  DROP TABLE cron_bank;

  RETURN result;
END
$$ LANGUAGE plpgsql;

COMMIT;

/* Yii English source translation table */
/* app\modules\lang\controllers\TranslationController uses this table */
CREATE TABLE "source_message" (
  id SERIAL PRIMARY KEY,
  category VARCHAR(32) DEFAULT 'app',
  message VARCHAR(256)
);
CREATE UNIQUE INDEX message_id ON "source_message" USING btree ("id");


/* Yii Russian target translation table */
/* app\modules\lang\controllers\TranslationController uses this table */
CREATE TABLE "message" (
  "id" INT,
  "language" VARCHAR(16) DEFAULT 'ru',
  "translation" VARCHAR(256),
  PRIMARY KEY (id, language),
  CONSTRAINT fk_message_source_message FOREIGN KEY (id)
  REFERENCES source_message (id) ON DELETE CASCADE ON UPDATE RESTRICT
);


/* English-Russian translation view */
CREATE VIEW "translation" AS
  SELECT s.id, message, translation
  FROM source_message s JOIN message t ON s.id = t.id
  WHERE "language" = 'ru';


/* User account, see app\models\User */
CREATE TABLE "user" (
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(24) NOT NULL UNIQUE,
  account DECIMAL(8,2) NOT NULL DEFAULT '0.00',
  email VARCHAR(48) NOT NULL UNIQUE,
  hash CHAR(60),
  auth CHAR(64) UNIQUE,
  code CHAR(64),
  perfect CHAR(8),
  status SMALLINT NOT NULL DEFAULT 2,
  ref_name VARCHAR(24),
  last_access TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  data BYTEA
);
CREATE UNIQUE INDEX user_id ON "user" USING btree ("id");
CREATE UNIQUE INDEX user_name ON "user" USING btree ("name");
CREATE UNIQUE INDEX user_email ON "user" USING btree ("email");

INSERT INTO "user"(name, email, status) VALUES ('admin', 'lab_tas@ukr.net', 1);


/* User action log, see app\models\Record */
CREATE TABLE "journal" (
  id SERIAL PRIMARY KEY NOT NULL,
  type VARCHAR(16) NOT NULL,
  event VARCHAR(16) NOT NULL,
  object_id INT,
  data TEXT,
  user_name VARCHAR(24),
  time TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  ip INET,
  CONSTRAINT journal_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE
);


/* Investments table, see app\modules\bank\models\Node */
CREATE TABLE "node" (
  id SERIAL PRIMARY KEY NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  amount DECIMAL(8,2) NOT NULL,
  "open" BOOL NOT NULL DEFAULT TRUE,
  time TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT node_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "amount" CHECK (amount >= 0)
);
CREATE UNIQUE INDEX node_id ON "node" USING btree ("id");


/* Payment (if amount > 0) and withdrawal (if amount < 0) table, see app\invoice\models\Invoice */
CREATE TABLE "invoice" (
  id SERIAL PRIMARY KEY NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  amount DECIMAL(8,2) NOT NULL,
  batch BIGINT,
  status VARCHAR(16) DEFAULT 'create',
  CONSTRAINT invoice_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT amount CHECK (amount <> 0)
);
CREATE UNIQUE INDEX invoice_id ON "invoice" USING btree ("id");


/* Site balance */
CREATE TABLE "account" (
  "profit" NUMERIC(8,2) NOT NULL DEFAULT 0
);
INSERT INTO "account" VALUES (0);


/* Income from investments log, see app\modules\bank\models\Income */
CREATE TABLE "income" (
  id SERIAL PRIMARY KEY NOT NULL,
  node_id INT NOT NULL,
  amount DECIMAL(8,2) NOT NULL,
  "time" TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT income_node FOREIGN KEY ("node_id")
  REFERENCES "node"(id)
);
CREATE UNIQUE INDEX income_id ON "income" USING btree ("id");


CREATE VIEW income_view AS
  (SELECT id, user_name, amount, "time" as "start"
   FROM node WHERE (NOW() - "time") > INTERVAL '1 days' AND "open")
  EXCEPT
  (
    SELECT id, user_name, amount, "start"
    FROM
      (SELECT node.id, user_name, node.amount, max(income."time") as "max", node."time" as "start"
       FROM income JOIN node ON node_id = node.id
         WHERE "open"
       GROUP BY node.id) t
    WHERE NOW() - "max" < INTERVAL '1 days'
  );


CREATE TABLE bonus (
  id SERIAL PRIMARY KEY NOT NULL,
  node_id INT,
  user_name VARCHAR(24) NOT NULL,
  amount DECIMAL(8,2) NOT NULL,
  "time" TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT bonus_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT amount CHECK (amount > 0)
);


/* User and guest feedback table for app\modules\feedback\models\Feedback */
CREATE TABLE "feedback" (
  id SERIAL PRIMARY KEY NOT NULL,
  username VARCHAR(24) NOT NULL,
  email VARCHAR(48),
  subject VARCHAR(256) NOT NULL,
  content TEXT NOT NULL
);
CREATE UNIQUE INDEX feedback_id ON "feedback" USING btree ("id");


CREATE TABLE "article" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(24),
  "title" VARCHAR(256) NOT NULL,
  "keywords" VARCHAR(192),
  "summary" TEXT,
  "content" TEXT NOT NULL
);
CREATE UNIQUE INDEX article_id ON "article" USING btree ("id");
INSERT INTO article(name, title, content) VALUES
  ('main', 'PROFIT 24', ''),
  ('about', 'О нас', ''),
  ('marketing', 'Маркетинг', ''),
  ('rules', 'Правила', ''),
  ('contacts', 'О нас', '');


CREATE TABLE "review" (
  id SERIAL PRIMARY KEY NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  content TEXT NOT NULL,
  CONSTRAINT review_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX review_id ON "review" USING btree ("id");


CREATE TABLE "faq" (
  "id" SERIAL PRIMARY KEY NOT NULL,
  "number" SMALLINT,
  "question" VARCHAR(256) NOT NULL,
  "answer" TEXT NOT NULL
);


CREATE TABLE "setting" (
  "category" VARCHAR(24) NOT NULL,
  "name" VARCHAR(24) NOT NULL,
  "type" VARCHAR(24),
  "value" TEXT
);

INSERT INTO "setting"("category", "name", "value") VALUES ('common', 'email', NULL);
INSERT INTO "setting"("category", "name", "value") VALUES ('common', 'referral', 0.1);
INSERT INTO "setting"("category", "name", "value") VALUES ('common', 'referral', 0.15);
INSERT INTO "setting"("category", "name", "value") VALUES ('common', 'login_fails', 5);
INSERT INTO "setting"("category", "name", "value") VALUES ('common', 'skype', NULL);
INSERT INTO "setting"("category", "name", "value") VALUES ('common', 'google', 'UA-58031952-7');
INSERT INTO "setting"("category", "name", "value") VALUES ('common', 'heart', '804734');

INSERT INTO "setting"("category", "name", "value") VALUES ('perfect', 'id', NULL);
INSERT INTO "setting"("category", "name", "value") VALUES ('perfect', 'password', NULL);
INSERT INTO "setting"("category", "name", "value") VALUES ('perfect', 'wallet', NULL);
INSERT INTO "setting"("category", "name", "value") VALUES ('perfect', 'secret', NULL);

INSERT INTO "setting"("category", "name", "value") VALUES ('deposit', 'min', 30);
INSERT INTO "setting"("category", "name", "value") VALUES ('deposit', 'max', 1000);
INSERT INTO "setting"("category", "name", "value") VALUES ('deposit', 'income', 0.1);
INSERT INTO "setting"("category", "name", "value") VALUES ('deposit', 'days', 10);
INSERT INTO "setting" VALUES ('deposit', 'auto', 'bool', 1);


/* see web/visit.php */
/* Visitors list */
CREATE TABLE "visit_agent" (
  "id" SERIAL PRIMARY KEY,
  "agent" VARCHAR(200),
  "ip" INET
);
CREATE INDEX visit_agent_agent ON "visit_agent" USING btree ("agent");
CREATE INDEX visit_agent_ip ON "visit_agent" USING btree ("ip");


/* Visitors log */
CREATE TABLE "visit_path" (
  "id" SERIAL PRIMARY KEY,
  "agent_id" INT NOT NULL,
  "path" VARCHAR(80) NOT NULL,
  "spend" SMALLINT,
  "time" TIMESTAMP DEFAULT current_timestamp,
  "data" TEXT,
  CONSTRAINT user_agent FOREIGN KEY (agent_id)
  REFERENCES "visit_agent"("id")
  ON DELETE CASCADE ON UPDATE CASCADE
);


/* Visitors with user agent log view */
CREATE VIEW "visit" AS
  SELECT p.id as id, agent_id, spend, user_name, "path", p."time", a.ip, agent FROM visit_path p
    JOIN visit_agent a ON agent_id = a.id
    LEFT JOIN journal j ON a.ip = j.ip and user_name is not null
    GROUP BY p.id, user_name, a.ip, agent;

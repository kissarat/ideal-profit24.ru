<?php

namespace app\models;


use app\helpers\SQL;
use PDO;
use Yii;
use yii\base\Model;

class Settings extends Model {

    private $_attributes = [];
    private $_types = [];
    public $category;
    private static $_all;

    public static function all() {
        if (!static::$_all) {
            static::$_all = Yii::$app->cache->get('settings');
            if (!static::$_all) {
                static::$_all = [];
                $command = Yii::$app->db->createCommand('SELECT "category", "name", "value" FROM setting');
                $command->execute();
                while($setting = $command->pdoStatement->fetch(PDO::FETCH_NUM)) {
                    static::$_all[$setting[0]][$setting[1]] = $setting[2];
                }
                Yii::$app->cache->set('settings', static::$_all);
            }
        }
        return static::$_all;
    }

    public static function get($category, $name) {
        return static::all()[$category][$name];
    }

    public static function getCategory($name) {
        return static::all()[$name];
    }

    public function setup($attributes) {
        foreach($attributes as $name => $value) {
            $this->$name = $value;
        }
    }

    public static function set($category, $name, $value, $reset = true) {
        $command = Yii::$app->db->createCommand('
          UPDATE "setting" SET "value" = :value
          WHERE "category" = :category AND "name" = :name', [
            ':category' => $category,
            ':name' => $name,
            ':value' => $value
        ]);
        $count = $command->execute();
        if ($count && $reset) {
            Yii::$app->cache->delete('settings');
        }
        return $count;
    }

    public function save() {
        $count = 0;
        foreach($this->_attributes as $name => $value) {
            $count += Settings::set($this->category, $name, $value, false);
        }
        Yii::$app->cache->delete('settings');
        return $count;
    }

    public static function categories() {
        return [
            'common' => Yii::t('app', 'Общие'),
            'perfect' => Yii::t('app', 'Perfect Money'),
            'deposit' => Yii::t('app', 'Investments')
        ];
    }

    public function attributes() {
        $names = array_keys(static::getCategory($this->category));
        $attributes = [];
        foreach($names as $name) {
            $attributes[] = $this->category . '_' . $name;
        }
        return $attributes;
    }

    public function attributeName($name) {
        return substr($name, strlen($this->category) + 1);
    }

    public function initAttributes() {
        $this->_attributes = $this->getCategory($this->category);
        $this->_types = SQL::queryAll('SELECT "name", "type" FROM setting WHERE "category" = :category', [
            ':category' => $this->category
        ], PDO::FETCH_KEY_PAIR);
    }

    public function clearAttributes() {
        foreach($this->_attributes as $name => $value) {
            $value = trim($value);
            if (is_string($value) && strlen($value) == 0) {
                $this->_attributes[$name] = null;
            }
        }
    }

    public function getTitle() {
        return static::categories()[$this->category];
    }

    public function getType($name) {
        return $this->_types[$this->attributeName($name)];
    }

    public function __get($name) {
        return $this->_attributes[$this->attributeName($name)];
    }

    public function __set($name, $value) {
        $this->_attributes[$this->attributeName($name)] = $value;
    }

    public function attributeLabels() {
        return [
            'common_email' => 'Email администратора',
            'common_referral' => 'Реферальное начисления',
            'common_team_referral' => 'Реферальное начисления лидеров',
            'common_login_fails' => 'Максимальное количество неудачных попыток входа за 5 минут',
            'common_skype' => 'Skype',
            'common_google' => 'Google Analytics',
            'common_heart' => 'Site Heart',

            'perfect_id' => 'ID',
            'perfect_password' => 'Пароль',
            'perfect_wallet' => 'Кошелек',
            'perfect_secret' => 'Альтернативний код',

            'deposit_min' => 'Минимальний депозит',
            'deposit_max' => 'Максимальний депозит',
            'deposit_income' => 'Прибыль',
            'deposit_days' => 'Срок депозита в днях',
            'deposit_auto' => 'Автоматическое открытия депозитов',
        ];
    }
}

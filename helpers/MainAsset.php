<?php

namespace app\helpers;

use yii\web\AssetBundle;

class MainAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'main.css'
    ];
    public $js = [
        'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/ru.js',
        'script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
